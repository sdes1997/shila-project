package shila;

import java.util.LinkedList;
import javafx.application.Application;
import shila.JavaApplication1;
import java.util.concurrent.CyclicBarrier;
import org.apache.commons.math3.stat.regression.*;
public class Room implements Runnable {

    private int id;
    static private Position positions[][];
    static int rows, cols;
    //estaria bé afegir llista de llums i positions (mentre no fem que home sigui el facade)
    private LinkedList<Light> lights;
    private LinkedList<Sensor> sensors;
    private LinkedList<Person> persons;
    private Microcontroller myArduino;
    public static CyclicBarrier barrier;
    private int lightBarrier = 0;
    public static Object o;
    SimpleRegression[] regression;

    public Room(int id, int numrows, int numcols) {
        persons = new LinkedList<>();
        this.id = id;
        cols = numcols;
        rows = numrows;
        positions = new Position[rows][cols];
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                positions[i][j] = new Position(i, j);
                positions[i][j].setRoom(this);
            }
        }
        lights = new LinkedList<>();
        sensors = new LinkedList<Sensor>();
        myArduino = new Microcontroller();
        o = new Object();
        

    }

    //return linked lis of lights
    public LinkedList<Light> getLights() {
        return lights;
    }
    
    public void initRegression(){
        regression = new SimpleRegression[persons.size()];
        for(int i = 0; i < persons.size(); i++){
            regression[i] = new SimpleRegression();
        }
    }
    
    public int getNumberOfLights(){
        return lights.size();
    }
    
    public void setLightBarrier( int n ){
        synchronized(this){
            lightBarrier = n;
        }
    }
    
    public int getLightBarrier(){
        return lightBarrier;
    }
    
    public void incrementBarrierCount(){
        synchronized(this){
            lightBarrier++;
        }
    }
    
    
    //return linked list of sensors

    public LinkedList<Sensor> getSensors() {
        return sensors;
    }

    public int getId() {
        return id;
    }

    public void addPerson(Person p) {
        persons.add(p);
    }

    public void removePerson(Person p) {
        persons.remove(p);
    }

    public LinkedList<Person> getPeople() {
        return persons;
    }
    
    public SimpleRegression getRegression(int correspondingPerson){
        return regression[correspondingPerson];
    }
    
    public void addPointsToRegression(int x, int y, int index){
        synchronized(regression[index]){
            regression[index].addData(x, y);
        } 
    }

    public Person searchPersonByPos(int x, int y) {
        for (Person p : persons) {
            if (p.getPos().getX() == x && p.getPos().getY() == y) {
                //System.out.println("person found");
                return p;
            }
        }
        //System.out.println("no person found :(");
        return null;
    }

    public Position getPosition(int idx, int idy) {
        return positions[idx][idy];
    }

    public static int Rows() {
        return rows;
    }

    public static int Cols() {
        return cols;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public Position[][] getPositionss(){
        return positions;
    }

    public void movePerson(Person p, Position pos) {
        p.changeCoord(pos);

    }

    public Position searchPosition(int x, int y) {
        //com que les posicions estàn ordenades només ens cal retornar el corresponent a lindex
        return positions[x][y];
    }
    
    public static Position[][] getFirstNeighbours(int x, int y){
        Position[][] poses = new Position[3][3];
        int k = -1, z = -1;
        for(int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                //check x and y boundaries
                if (x + k < Rows() && y + z < Cols() && x + k >= 0 && y + z >= 0){
                    poses[i][j] = positions[x+k][y+z];
                }
                z++;
            }
            k++;
            z = -1;
        }
        return poses;
    }
    
    public static LinkedList<Position> getSecondNeighbours(int x, int y){
        LinkedList<Position> list = new LinkedList<>();
        if (x < Rows() && y + 1 < Cols())
            list.add(positions[x-2][y+2]);
        if (x - 1 < Rows() && y + 2 < Cols())
            list.add(positions[x-1][y+2]);
        if (x < Rows() && y + 2 < Cols())
            list.add(positions[x][y+2]);
        if (x + 1 < Rows() && y < Cols())
            list.add(positions[x+1][y]);
        if (x + 2 < Rows() && y + 2 < Cols())
            list.add(positions[x+2][y+2]);
        if (x + 2 < Rows() && y + 1 < Cols())
            list.add(positions[x+2][y+1]);
        if (x + 2 < Rows() && y < Cols())
            list.add(positions[x+2][y]);
        if (x + 2 < Rows() && y - 1 < Cols())
            list.add(positions[x+2][y-1]);
        if (x + 2 < Rows() && y - 2 < Cols())
            list.add(positions[x+2][y-2]);
        if (x + 1 < Rows() && y - 2 < Cols())
            list.add(positions[x+1][y-2]);
        if (x < Rows() && y - 2 < Cols())
            list.add(positions[x][y-2]);
        if (x + 1 < Rows() && y - 2 < Cols())
            list.add(positions[x+1][y-2]);
        if (x + 2 < Rows() && y - 2 < Cols())
            list.add(positions[x+2][y-2]);
        if (x + 2 < Rows() && y - 1 < Cols())
            list.add(positions[x+2][y-1]);
        if (x + 2 < Rows() && y < Cols())
            list.add(positions[x+2][y]);
        if (x + 2 < Rows() && y + 1 < Cols())
            list.add(positions[x+2][y+1]);
        return list;
    }
    
    public int getMaxDistanceFromPerson(int x, int y){
        int max = 0;
        int current;
        for (Person p : persons){
            current = (p.getPos().getX() - x) * (p.getPos().getX() - x)
                    + (p.getPos().getY() - y) * (p.getPos().getY() - y);
            if (current > max && max < 10)
                max = current;
        }
        return max;
    }

    //printa la matriz de posiciones con sus respectivas intensidades
    public void printMatrixIntensities() {
        Position pos;
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                pos = positions[i][j];
                synchronized (positions[i][j].getSensor().getLight()) {
                    System.out.print(pos.getLightIntensity() + "  ");
                }
                //pos.getSensor().getLight().setIntensity(1);
                //System.out.print(pos.getLightIntensity()+ "  ");
            }
            System.out.println();
        }
    }
    
    public int isCorridor(){
        // si és corredor de rows torem 1 , si es de cols tornem 2 si no és tornem 0
        if(rows < 2){
            return 1;
        } else if(cols < 2){
            return 2;
        }
        else return 0;
    }

    public void printPersonStatus() {
        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                Person p = positions[i][j].getPerson();
                if (p != null) {
                    System.out.print("x     ");
                    // System.out.print(positions[i][j].getSensor().getState());
                } else {
                    System.out.print("0     ");
                }
            }
            System.out.println();
        }
        

    }
    
    public static Position[][] getPositions(){
            return positions;
        }

    public void run() {

        new Thread() {
            //@Override
            public void run() {
                javafx.application.Application.launch(JavaApplication1.class);
            }
        }.start();
        JavaApplication1 startUpTest = JavaApplication1.waitForStartUpTest();
        JavaApplication1.colflag.set(cols);
        JavaApplication1.rowflag.set(rows);
        //init the barrier to get the lights
        barrier = new CyclicBarrier(lights.size() + persons.size(), new Runnable(){
            public void run(){
                printMatrixIntensities();
                printPersonStatus();
            }
        });
        for (Person per : getPeople()) {
            (new Thread(per)).start();
        }
        for (Sensor s : getSensors()) {
            (new Thread(s)).start();
        }
    }
}
