package shila;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.concurrent.BrokenBarrierException;

public class MovementSensor extends Sensor {

    //private int id; ...
    private int state; //1->detecta moviment, 0->no en detecta
    
    public MovementSensor(int id, Position pos) {
        super(id, pos);
        this.state = 0;//perque en principi no detecta moviment

    }

    //determina state segun el estado que ha detectado el sensor (1)
    public synchronized void setMovement(int state) {
        this.state = state;
    }
    
    public boolean checkSurroundingPersons(){ //mirar si tenim una persona en una posició contigua
            Room room = myPos.getRoom();
        
            int x= myPos.getX();
            int y= myPos.getY();
            int rows=myPos.Rows();
            int cols=myPos.Cols();
            
            for(int k = x-1; k <= x+1; k++){//bucle de 3 en el cas d'una matriu 3x3
                for(int l = y-1; l<= y+1; l++){                 
                    if(!((k == x) && (l == y))){ //si son les posicions (x i y) del sensor que ha detectat ens saltem l'if
                    
                        if(k>=0 && l>=0 && k<rows && l<cols ){
                                if(myPos.getRoom().searchPersonByPos(k, l)!=null){ 
                                    return true;//hi ha persona
                                }
                        }
                    }
                }
            }
           
           return false; //si no es troba una persona en les posicions contigues.                        
                              
        
    }


    @Override
    public int getState() { //ha de retornar 1 si rep moviment i 0 si no en rep. per llegiro per la simulació ho podem fer de una base de dades.
        return this.state;
    }

    //encendemos la luz que tiene el sensor que ha detectado 1 con la intensidad deseada
    @Override
    public synchronized void turnOnLight() {
        light.setIntensity(0.9);
    }
    
    //Now we take into account the current light to calculate the intensity
    
    /*public double setNewIntensity(double x, int dis){
        double intensity = (3 - x) * (1 + dis/10);
        if (intensity < 0) return 0; 
        return intensity;
    }*/
    
    public double setNewIntensity(double x){
        double intensity = (3 - x); 
        return intensity;
    }
    
    public void getLightsFromRegression(){
        double oldIntensity;
        //set the points to the regression data
        myPos.getRoom().addPointsToRegression(myPos.getX(), myPos.getY(), myPos.getPerson().getId());
        //myPos.getRoom().getRegression(myPos.getPerson().getId()).addData(myPos.getX(), myPos.getY() );
        // get the equation of the line
        double m = myPos.getRoom().getRegression(myPos.getPerson().getId()).getSlope();
        // imagine we're at 0,0, as we just need to get the unity vectors of the regression line
        int y = (int)myPos.getRoom().getRegression(myPos.getPerson().getId()).predict(1);
        int newX = (int)m;
        int newY = (int)y;
        // now that we have the points, lets get the unity vectors 
        double dist = Math.sqrt(m*m + y*y);       
        double dirX = Math.ceil(m/dist);
        double dirY = Math.ceil(y/dist);
        //we have to sum 1 as position 1,1 of neighbours is ours
        Position positionToOpen = myPos.getNeighbours()[1 + (int)dirX ][ 1 + (int)dirY ];
        // now we have to tell the lights in the position to open
        if (positionToOpen != null){
            oldIntensity = positionToOpen.getLightIntensity();
            positionToOpen.getSensor().getLight().setIntensity(0);
            positionToOpen.setPositionLightIntensity(0);
            positionToOpen.getSensor().getLight().setIntensity(setNewIntensity(oldIntensity));
               // change the light that's in the surroundings of the one we've notified
            positionToOpen.addLight(setNewIntensity(oldIntensity));
            positionToOpen.setNeighboursLights(oldIntensity);
        }
       
        
    }

    @Override
    public void run() {
        //LinkedList<Position> poses = Room.getFirstNeighbours(myPos.getX(), myPos.getY());
        //Position[][] poses = Room.getFirstNeighbours(myPos.getX(), myPos.getY());
        while (true) {
            try {
                double oldintensity = myPos.getLightIntensity();
                light.setIntensity(0);
                myPos.setPositionLightIntensity(0);
                myPos.getRoom().incrementBarrierCount();
                if (myPos.getRoom().getLightBarrier() == myPos.getRoom().getNumberOfLights()) {
                    // if this is the last element set the barrier to 0
                    myPos.getRoom().setLightBarrier(0);
                    synchronized (Room.o) {
                        Room.o.notifyAll();
                    }
                }
                else{
                    synchronized (Room.o) {
                        Room.o.wait();
                    }
                }
                
                if (hasPerson()) {
                    //light.setIntensity(setNewIntensity(oldintensity, myPos.getRoom().getMaxDistanceFromPerson(myPos.getX(),myPos.getY())));
                    
                    //lock it
                    myPos.addLight(setNewIntensity(oldintensity));
                    // perform regression in order to open the next lights
                    
                    // set the natural intensity of the neighbours
                    myPos.setNeighboursLights(oldintensity);   
                    getLightsFromRegression();
                }
                
                //notifiquem a la barrera que ja estem
                Room.barrier.await();
                Thread.sleep(30);
            } catch (InterruptedException | BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

}
