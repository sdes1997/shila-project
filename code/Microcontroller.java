/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shila;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author DS\u137544
 */
public class Microcontroller {
    
    boolean isNight(){
        
        int from = 2200;
        int to = 800;
        
        Date date = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        
        int t = c.get(Calendar.HOUR_OF_DAY) * 100 + c.get(Calendar.MINUTE);
        boolean isBetween = to > from && t >= from && t <= to || to < from && (t >= from || t <= to);
    
        return isBetween;
    }

    
    
}
