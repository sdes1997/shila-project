package shila;

import java.util.*;

public class Home {

    private LinkedList<Room> rooms;
    private LinkedList<Person> persons;
    private LinkedList<Light> lights;
    private LinkedList<Sensor> sensors;

    public Home() {
        rooms = new LinkedList<>();
        persons = new LinkedList<>();
        lights = new LinkedList<>();
        sensors = new LinkedList<>();
    }

    // adds i els gets de les llistes ...
    public Room getRoom(int idx) {
        return rooms.get(idx);
    }
    
    public void addRoom(Room r){
        rooms.add(r);
    }
    
    public void addPerson(Person p){
        persons.add(p);
    }
    
    // funció per inicialitzar més ràpidament, li passem la habitació que volem
    // llavors la funció s'assegura de posar un sensor i una llum cada dues posicions
    public void initLightSensor(){
        int j = 0; 
        int k = 0;
        int i = 0;
        for (Room r : rooms) {
            while (k < r.Rows()) {
                //instanciar llum
                Light l = new Light(i);
                //instanciar sensor
                Sensor s = new MovementSensor(i, r.getPosition(k, j));
                // afegir llums al sensor
                s.setLight(l);
                // afegir llum a la llista de la room
                r.getLights().add(l);
                // afegir sensor a la llista de la room
                r.getSensors().add(s);
                // afegir llum a la llista de home
                lights.add(l);
                // afegir sensor a la llista de home
                sensors.add(s);
                // linkar la posició amb el sensor
                r.getPosition(k, j).setSensor(s);
                //Anar canviant posicions. La funció està pensada per posar llums
                //cada dues posicions
                j += 1;
                if (j >= r.Cols()) {
                    j = 0;
                    k += 1;
                }
                i++;
            }
        }
        
    }

    /*...*/

    public static void main(String args[]) {
        Home h = new Home();
        int numRooms = Integer.parseInt(args[0]);
        int numPeople = Integer.parseInt(args[1]);
        //--rand = random movement, --linear = lineal movement, --snake = snake movement
        String movementAlg = args[2];
       
        for(int i = 0; i < numRooms; i++){
            Room r = new Room(i, 10, 10);
            h.addRoom(r);
        }
        h.initLightSensor();
        for(Room r : h.rooms){
            for (int i = 0; i < numPeople; i++) {
                int highcols = r.Cols();
                int highrows = r.Rows();
                Random rand = new Random();
                int col = rand.nextInt(highcols);
                int row = rand.nextInt(highrows);
                Position pos = r.getPosition(row, col);
                Person p = new Person(i, pos, movementAlg);
                h.addPerson(p);
                r.addPerson(p);
            }
            r.initRegression();
        }

        for(Room r : h.rooms){
            for(int i = 0; i < r.Rows(); i++){
                for(int j = 0; j < r.Cols(); j++){
                    r.getPosition(i,j).setNeighbours(r.getFirstNeighbours(i,j));
                }
            }
            //set the first points to start the regression
            for(Person p: r.getPeople()){
                r.getRegression(p.getId()).addData(0,0);
            }
            
        }
        
        
        //fem correr les instàncies de persona
        for (Room r : h.rooms) {
            (new Thread(r)).start();
        }    
    }
}
