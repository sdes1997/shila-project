package shila;

public class Light{

    private int id;
    private double intensity; //intensity=0->llum apagada|| intensity=1->llum a maxima intensitat.
    //cada llum té un lock connectat amb el sensor de manera que es pugui notificar
    public boolean lock;

    public Light(int id) {
        this.id = id;
        intensity = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getIntensity() {
        return this.intensity;
    }

    public void setIntensity(double i) {
        intensity = i;
    }
}
