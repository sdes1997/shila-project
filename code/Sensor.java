package shila;

public abstract class Sensor implements Runnable {

    protected boolean infinit; //just for infinit loop
    protected int id;
    protected Light light;
    protected Position myPos;
    //protected LockLightSensor lock;

    public Sensor(int id, Position pos) {
        this.id = id;
        myPos = pos;
        //light = new Light(id);
        infinit = true;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setLight(Light l) {
        light = l;
    }

    public void setPosition(Position p) {
        myPos = p;
        //myPos.setSensor(this);
    }
    
    public Position getPosition(){
        return myPos;
    }

    public Light getLight() {
        return light;
    }

    public boolean hasPerson() {
        // Person p = myPos.getRoom().searchPersonByPos(myPos.getX(), myPos.getY());
        Person p = myPos.getPerson();
        if (p == null) {
            return false;
        }
        return true;

    }

    //metodes de Movement Sensor
    public abstract int getState();

    public abstract void turnOnLight();

    @Override
    public void run(){
        //doesnt do anything
        /*while (true) {
            try {
                
                light.setIntensity(0);
                
                if (hasPerson())
                    light.setIntensity(1);
                

                
                //LockObj.LockSensorLight.wait();
                //turnOff();
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    */}

}
