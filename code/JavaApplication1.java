package shila;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicInteger;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.util.Duration;


/**
 *
 * @author DS\u137505
 */
public class JavaApplication1 extends Application {

    int rowNum = 15;
    int colNum = 15;
    int recSize = 50;
    public static AtomicInteger colflag = new AtomicInteger();
    public static AtomicInteger rowflag = new AtomicInteger();
    public static final CountDownLatch latch = new CountDownLatch(1);
    public static JavaApplication1 startUpTest = null;
    private Position positions[][];

    public static JavaApplication1 waitForStartUpTest() {
        
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return startUpTest;
    }

    public static void setStartUpTest(JavaApplication1 startUpTest0) {
        startUpTest = startUpTest0;
        latch.countDown();
    }
    
    public void setLength(int x){
        rowNum = x;
    }
    
    public void setWidth(int y){
        colNum = y;
    }

    public JavaApplication1() {
        setStartUpTest(this);
    }
    
    public Color chooseColor(int x, int y) {
        Color c = Color.RED;
        if (positions[x][y].getPerson() != null)
            return Color.BLACK;
        else{
            int redintensity = (int) (150 + 35 * positions[x][y].getLightIntensity());
            if (redintensity > 255)
                redintensity = 255;
            if (redintensity < 0) redintensity = 150;
            c =  Color.rgb(redintensity, 0, 0);
            return c;
        }
            
        
        
    }

    

    @Override
    public void start(Stage primaryStage) throws Exception {
        
        positions = Room.getPositions();
              
        GridPane grid = new GridPane();
  
        
        rowNum = rowflag.get();
        colNum = colflag.get();      
        
        
        recSize = 800 / rowNum;
        if (recSize == 0)
            recSize = 1;

        for (int row = 0; row < rowNum; row++) {
            for (int col = 0; col < colNum; col++) {
                //int n = rand.nextInt(4);
                Rectangle rec = new Rectangle();
                rec.setWidth(recSize);
                rec.setHeight(recSize);
                rec.setFill(chooseColor(row,col));
 /*               
                Light.Spot light = new Light.Spot();
                light.setColor(Color.YELLOW);
                if (positions[row][col].getSensor().getLight().getIntensity() == 2){
                    light.setX(row * recSize + recSize / 2);
                    light.setY(col * recSize + recSize / 2);
                    Lighting lighting = new Lighting();
                    lighting.setLight(light);
                    rec.setEffect(lighting);
                }
 */               
                
                GridPane.setRowIndex(rec, row);
                GridPane.setColumnIndex(rec, col);
                grid.getChildren().addAll(rec);
            }
        }
        
        //System.out.println(rowflag);
        //System.out.println(colflag);
        
        //put a lock here until the room says it has input the sizes?

        Scene scene = new Scene(grid, recSize * colflag.get() , recSize * rowflag.get() );

        primaryStage.setTitle("SHILA");
        primaryStage.setScene(scene);
        primaryStage.show();

        Timeline timeline = new Timeline(new KeyFrame(
                Duration.millis(2000),
                ae -> {
                    positions = Room.getPositions();
                    for (int row = 0; row < rowNum; row++) {
                        for (int col = 0; col < colNum; col++) {
                            //int n = rand.nextInt(4);
                            Rectangle rec = new Rectangle();
                            rec.setWidth(recSize);
                            rec.setHeight(recSize);
                            rec.setFill(chooseColor(row,col));
                            
/*
                            Light.Spot light = new Light.Spot();
                            light.setColor(Color.YELLOW);
                            if (positions[row][col].getSensor().getLight().getIntensity() == 2){
                                light.setX(row * recSize + recSize / 2);
                                light.setY(col * recSize + recSize / 2);
                                Lighting lighting = new Lighting();
                                lighting.setLight(light);
                                rec.setEffect(lighting);
                            }
*/                
                            
                            GridPane.setRowIndex(rec, row);
                            GridPane.setColumnIndex(rec, col);
                            grid.getChildren().addAll(rec);
                        }
                    }
                }));
        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();

    }

    public static void main(String[] args) {
        launch(args);
    }
}
