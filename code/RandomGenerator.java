package shila;

import java.util.Random;
import static shila.Weather.CLOUDY;
import static shila.Weather.RAINY;
import static shila.Weather.SUNNY;

enum Weather {
    SUNNY, RAINY, CLOUDY;
}

public class RandomGenerator {

    public Weather getWeather() {
        //depenent del número que surti tindrem un weather o un altre
        Random rand = new Random();
        int n = rand.nextInt(3) + 1;

        if (n == 0) {
            return SUNNY;
        }
        if (n == 1) {
            return RAINY;
        }
        if (n == 2) {
            return CLOUDY;
        } else {
            return null;
        }
    }

    public int[] getPositionsRandomly() {
        // els valors poden estar entre -2 i 2 que fan una matriu 3x3
        // de posicions adjacents
        int high = 2;
        int low = -2;
        int pos[] = new int[2];
        Random rand = new Random();
        for (int i = 0; i < 2; i++) {
            pos[i] = rand.nextInt(high - low) + low;
        }
        // per simplicitat, retornem un array de posicions
        return pos;
    }
    
    public int[] getPositionsStraightLine( boolean goingBack){
        int ret[] = new int[2];
        ret[1] = 0;
        if (!goingBack){
            ret[0] = 1;
        }
        else{
            ret[0] = -1;
        }
        
        return (ret);
    }
    
    public int[] getPositionsSnake(int actualX, int actualY, int maxX, int maxY){
        int ret[] = new int[2];
        if(actualX % 2 == 0){
            if( actualY < maxY - 1 )
            {
                ret[0] = 0;
                ret[1] = 1;
            }
            else{
                ret[0] = 1;
                ret[1] = 0;
            }
        }
        else{
            if( actualY > 0 )
            {
                ret[0] = 0;
                ret[1] = -1;
            }
            else{
                ret[0] =  +1;
                ret[1] = actualY;
            }
            
        }
        if( actualX == maxX && (actualY == 0 || actualY == maxY)){
            ret[0] = -1;
        }
        return ret;
    }

}
