package shila;


import java.util.concurrent.BrokenBarrierException;

public class Person implements Runnable {

    private final int id;
    private Position myPos;
    private String movement;
    private boolean goingBack;
    private RandomGenerator randi;

    public Person(int id, Position pos, String typeOfMovement) {
        this.id = id;
        myPos = pos;
        movement = typeOfMovement;
        goingBack = false;
        randi = new RandomGenerator();
    }
    
    public int getId(){
        return id;
    }

    public void changeCoord(Position pos) {
        myPos = pos;
    }

    public Position getPos() {
        return myPos;
    }
    
    public int[] getPositionFromRand(){
        int pos[];
        if (movement == "--snake"){
            return randi.getPositionsSnake(myPos.getX(), myPos.getY(), myPos.Rows(), myPos.Cols());
        }else if (movement == "--rand"){
            return randi.getPositionsRandomly();
        }else{
            if(myPos.getX() == 0){
                goingBack = false;
            } else if(myPos.getX() == myPos.getRoom().Rows() - 1){
                goingBack = true;
            }
            return randi.getPositionsStraightLine(goingBack);
        }
    }
//para que sirven estas dos funciones?
/*
    public void close() {
        opened = false;
    }

    public void open() {
        opened = true;
    }
*/
    
    
    public void computeNewPos() {
        //pos[0] stands for the random x value
        //pos[1] stands for the random y value
        /*as these values are between -2 and 2 we have to add them 
        to the actual coordenates*/

        int newX = -1;
        int newY = -1;

        while (newX < 0 || newY < 0 || newX >= myPos.Rows() || newY >= myPos.Cols()) {
            //int pos[] = randi.getPositionsRandomly();
            //int pos[] = randi.getPositionsStraightLine();
            int pos[] = getPositionFromRand();
            newX = myPos.getX() + pos[0];
            newY = myPos.getY() + pos[1];
        }
        //buscar les noves coordenades a partir del randomitzador
        Position newPos = myPos.searchPosition(newX, newY);
        if (newPos.getPerson() == null){
            //eliminem la persona de la posició
            myPos.setPerson(null);
            //actualitzem la posició de la persona (setPos)
            changeCoord(newPos);
            //afegim persona a la nova posició
            myPos.setPerson(this);
        }
    }

    public void run() {
        while (true) {
            try {
                computeNewPos();
                //poso l'sleep aquí per assegurar-me que la intensitat sempre
                // s'agafi bé
                Thread.sleep(2000);
                Room.barrier.await();  
            } catch (InterruptedException |  BrokenBarrierException e) {
                e.printStackTrace();
            }
        }
    }

}
