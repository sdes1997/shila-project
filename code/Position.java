package shila;



public class Position {

    private Room room;
    private int x;
    private int y;
    private Person person;
    private Sensor sensor;
    private double lightIntensity;
    // les posisions veines
    private Position[][] neighbours;


    public Position(int x, int y) {
        this.x = x;
        this.y = y;
        lightIntensity = 0;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Room getRoom() {
        return room;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public void setSensor(Sensor sensor) {
        this.sensor = sensor;
        setLightbulbIntensity(sensor.getLight().getIntensity());
        this.sensor.setPosition(this);
    }

    public void setRoom(Room r) {
        room = r;
    }

    public Sensor getSensor() {
        return sensor;
    }

    public double getLightIntensity() {
        return lightIntensity;
    }

    public void setLightbulbIntensity(double intensity) {
        this.lightIntensity = intensity;
        sensor.getLight().setIntensity(intensity);

    }
    
    public void setPositionLightIntensity(double intensity) {
        synchronized (this) {
            this.lightIntensity = intensity;
            //sensor.getLight().setIntensity(intensity);
        }

    }
    
    public double setNewIntensity(double x){
        return (3 - x);
    }
    
    // aquesta funció determina la intensitat de la llum a una posició   
    public void setNeighboursLights(double oldintensity) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (neighbours[i][j] != null) {
                    if (neighbours[i][j].getPerson() == null) {
                        //lock it
                        neighbours[i][j].addLight(setNewIntensity(oldintensity) * 0.5); 
                    }
                }
            }
        }

    }

    public void addLight(double in){
        synchronized(this){
            this.lightIntensity += in;
        }  
    }

    public Position searchPosition(int x, int y) {
        return (room.searchPosition(x, y));
    }

    public int Rows() {
        return room.Rows();
    }

    public int Cols() {
        return room.Cols();
    }

    public Person getPerson() {
        return person;
    }
    
    public Position[][] getNeighbours(){
        return neighbours;
    }
    
    public void setNeighbours(Position[][] poses){
        neighbours = poses;
    }
    
    
    // funcions no acabades per intentar avisar la següent llum
    public void setNextRowLight(){
        for(int j = 0; j < 2; j++){
            //if(neighbours[j][1])
        }
    }
    
    public void setNextColLight(){
        
    }

    //ajustamos las luces de alrededor
   /* public void adjustSurroundingLights() {
        room.refreshPos(x, y);
    }*/
}
