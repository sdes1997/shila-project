![Logo Shila](https://bitbucket.org/repo/64eex6g/images/1310266728-logomini.png)


Welcome to **Shila** - the open-source, parallel and distributed smart home lighting system.

This wiki is the main source of documentation for **developers** working with (or contributing to) the Shila project. If this is your first time hearing about Snowplow, we recommend starting with the [Shila website](https://shila.site) and our vídeo  at [Vimeo](https://vimeo.com/275633301)

# README #

This repository contains the files from our project of the subject "Introduction to parallel and distributed programming". In this readme you'll find all the information related to de project itself. 


### Changes  Java Project Simulation###
* **Aqui tots varem fer feines inicials però no estan documentades**

* **Nerea-20/05**
              
	* **Light**: setInitialIntensity cambiada a setIntensity porque la intensidad inicial cada vez que se crea una luz debe ser 0 y esto va en el constructor, y luego si se quiere cambiar la intensidad por supuesto que debe haber un setter, pero no una función que se llame setInitialItensity.

	* **MovementSensor**:La siguiente función está comentada por que ya había otra que hacia lo mismo que básicamente es la que llama en ella (setMovement). A si que me parece inútil. Esta comentada por si alguien argumenta otra cosa.

	* **Sensor**: Como dijimos en la reunión, cada sensor tiene una luz, asi que he añadido un atributo Light a Sensor, que cuando uno detecte movimiento se comunicará directamente con ésta luz para activarla.

	* **Room**: He cambiado el main de Room a Home como hablamos en la reunión. He añadido una función printMatrixIntensities que printa la matriz de posiciones con sus respectivas intensidades

	* **Position**: He añadido setLightIntensity y adjustSurroundingLights y me falta acabar ésta última

* **Lluis i Nerea 20/05**

	* **Room i Position**: Hem fet diversos canvis poc significatius, i a part d’això hem mirat d’implementar l’actualització de les llums segons l’estat del sensor d’una posició. Al principi hem intentat fer-ho com amb el north south de Enginyeria del Software fins que ens hem adonat que creavem posicions diferents pero amb les mateixes coordenades. Ho hem canviat fent que l’actualització es faixi desde la room, on tenim la matriu ja feta de posicions, i on canviem l’estat de les llums al voltant de unes coordenades que es passen com a paràmetre per a la funció. La idea es que la funció es cridi des de el home amb un bucle que recorre totes les posicions de la matriu. d’aquesta manera s’actualitzaria l’estat de les llums de tota la matriu.
Idea de paralelització: sería, amb openMP, fer un #pragma per posició en la matrriu de posicions de cada room.
	* **NOTA**: per a nosaltres, anyadir un if que digui que si tenim dues persones en posicions contigues, la llum s’actualitzi be. per fer això l’if hauria de comprovar els sensors de les posicions contigues de (x,y)
* **Lluis i Nerea 21/05**

	* **Sensor**: s'han afegit un atribut light, funcio getLight i funció abstracta getMovement.
	* **Room**: Petites modificacions, funcions que retornen llistes.
	* **Home**: S'han afegit a les llistes de llums, les llums apropiades (modificació general del main).
	* **General**:Hem fet que tot complili, resolts errors que ens donava pel fet de canviar el main de Room a Home (on hauria d'estar). Juntat també amb la branca del Gerard.
	
* **Gerard, Sandor, Aitor 22-05-2018**
	* Hacer que las personas cambien de posición usando un rand.
	* Conseguir que el sensor detecte si la persona esta debajo o no.
		*Hemos añadido una linkedList a cada Room de manerá que las personas se añaden al room y no ha el home en si.

	* Hemos sincronizado ambas cosas para que no haya problemas con un lock.

	* **Nota**: Molaría añadie un rango de detección a los sensores y no solo su propia ubicación.

* **Nerea i Lluis 26-05-2018**
	* **Home**: Es varen afegir llums i sensors al main per a fer la matriu 3*3 i es varen crear els threads necessaris per a correr el programa.
	* **Light**: Es varen modificar algunes funcions generals.
	* **MovementSensor**: Es van sincronitzar i modificar algunes funcions incloent el Run.
	* **Position**: Modificats SetLightIntensity (afegit que desde el sensor es cridi la llum i es pugui modificar la intensitat per a que no es cridi a doble banda). AdjustSurroundingLights es crida a la Room per a que cridi RefreshPos.
	* **Room**: RefreshPos completament modificada, hi havia coses que fallaven, adaptada per saltar-se la mateixa posició del sensor. Afegit tot el Run ade Room.
	

* **Gerard 27-05-2018**

	* Intentat posar una instància bloquejable a light, de manera que hi hagi una comunicació entre el thread de llum amb el corresponent thread de sensor. Sense aparent èxit però crec que la intuició és bona. 

	* Ara hi ha threads de llum més o menys arreglats tot i que falta aclarar com s'obren les llums (agafant una intensitat anteriorment posada per sensor?)

	* A aspectes pràctics, he refactoritzat el codi, ara està indentat correctament, i he creat una nova classe LockLightSensor, que inclou un enumerador a bloquejar. També se li ha canviat el nom a l'altre lock enum.

* **Gerard 28-05-2018** 
	* Ara en prinicipi la comunicació entre cada sensor i la seva llum és correcte. He descomentat alguns blocs de codi que em semblaven inoportuns i no entenia, de totes maneres, he debugat i ara les llums s'encenen i s'apaguen mitjançant notificacions amb condicions (a mode de monitors), la part bona és que a més es fa correctament amb tots els sensors. 

	* **Nota** Hem de fer una funció que ens permeti inicialitzar tots els sensors i llums de forma automàtica, sinó serà impossible fer una matriu de moltes posicions a aspectes pràctics. 

* **Nerea i Lluis 29-05-2018**
	* **Home**: Canviat nom de rows i cols per millor enteniment i modificades altres classes amb el matèix motiu. Matriu passada de 5*5 a 3*3. 
	* **LockNeighbourSensor**: creada classe, per a poder fer els locks (LockNeighLightOn/Off)
	* **MovementSensor i Room**:comentada la funció RefreshPos de Room, implementat millor a MovementSensor , que es desde alla on es parlaran els threads. La nova funció de MovementSensor te com a nom CheckSurroundingPersons, i el que fa es mirar si tenim una persona a una posició contigua. Al Run es va afegir un if, que diu que si no este una persona al voltant, s'envia una senyal d'apagat al llum del sensor, i si no (tenim persones al voltant) posem l'intensitat a 0.5.
	

* **Gerard 29-05-2018**
	* Afegit un condicional a la funció _run_ de _Light_ de manera que ara la sincronització és més correcta.
	* Canvis importants a _Home_ i al _main_, on ara tenim una funció que permet afegir sensors i llums automàticament. Al _main_ ara la creació de threads també està automatitzada de manera que ara és tot més fàcil d'emprar. 
	* **Nota**: Recordeu que cada vegada s'ha d'instanciar a _Home_ cosa que no fèiem anteriorment i que ara és primordial. 

* **Redistribució 30-05-2018**
	* **Es canvien les tasques, Gerard, Sándor i Aitor s'encarreguen de la simulació Java amb threads, Nerea i Lluis arduino distribuit, i Aitor documentació.**
	
* **Nerea i Lluis 02-06-2018**
	* **Afegit readme d'arduino**

* **Gerard 05-06-2018**
	* La classe **Light** ja no implementa més a *Runnable* i ja no corre com a thread.
	* Això es deu al fet que la intensitat es modifica desde **MovementSensor**.  
	* Ara **Room** és capaç de printar correctament les posicions i les intensitats gràcies a la implementació de *CyclicBarrier*.
	
	
### Changes  Arduino Simulation###

* **Lluis Preparació prèvia **
	* Al principi, ja que no havíem fet mai res relacionat amb arduino, em va dur unes hores aprendre
com funcionava tot el sistema, tant la part de hardware, pel que es necessiten coneixements
d’electrònica, com la part de software, d’instal·lar l’IDE d’arduino, aprendre com es programa i com
funcionen les funcions, i trobar una plataforma online (s’ha emprat tinkercad) on poder fer les
simulacions de hardware abans d’implementar-les en físic.
Per aquesta preparació prèvia es van mirar molts tutorials d’arduino per arribar als coneixements
necessaris.


* **Lluis Update **

	*  Després d’haver assolit aquests coneixements previs necessaris, es va comprar un arduino, junt
amb un sensor PIR de moviment per a fer proves del projecte, i es va fer la primera simulació al
tinkercad amb una llum, es va programar que s’encenés intermitentment i després es va passar a
hardware. ![img](https://s33.postimg.cc/ckh96b9tr/htrhthtrtr.png)



* **Lluis Update **

	* El seguent dia es va provar de fer el codi del sensor amb una llum, i que quan el sensor detectes
moviment la llum s’encenés. Es va fer la simulació de hardware al tinkercad per a no cremar res
en físic i després es va passar a físic. Va costar però al final es va aconseguir que funcionés
correctament i la llum s’encenes quan detectes moviment, i s’apagues quan un temps després.
https://www.tinkercad.com/things/gpsLWSHSVA2-sensor-de-moviment-amb-llum/editel?sharecode=kDUvT7afNWoTvaOC-6auy2XROW-XFVk0nd1xKFL7rdk=
vidio:
https://drive.google.com/file/d/1Zt-ehhV-rf403uOg5ydSpGOlNYLa9km2/view?usp=sharing  
![img](https://s33.postimg.cc/kpzb4l0of/e_ewr_wer.png)  


* **Lluis Update **
	*  A continuació, per a una millor implementació del codi i per a jugar amb les intensitats com volíem,
es va tenir que fer un cert canvi amb el codi per a poder fer que s’escribis informació analògica i
no digital a la llum per tal de poder fer que s’encenés amb l’intensitat de llum desitjada. per això es
va implementar la funció setIntensity, junt amb un petit canvi al hardware per conectar la llum a
una entrada analògica. (També es va necessitar recerca previa de tot el mon de l’entrada
analògica).  


* **Lluis Update **
	*  Després de donar una pausa a l’Arduino per fixar-nos en la simulació en java, es va seguir amb fer
la simulació en hardware (amb el tinkercad) d’un arduino amb 4 sensors i 4 llums, ja simulant una
habitació de 2*2. Més endavant ja s’intentarà generalitzar codi, però en aquest punt l’objectiu es la
funcionalitat. Es va començar a fer el codi en c però no es va acabar.  
	
* **Lluis Update **
	*  Aquí s’unèix la Nerea al tema arduino. Jo vaig acabar el codi en c de 4 sensors i 4 llums amb un
arduino i funcionava a la simulació del tinkercad .No implementat amb físic perquè no tenim
suficients sensors. Ens estem intentant posar en contacte amb el HackLab per a que ens
proporcionin algunes eines.  

	
* **Nerea Update **
	*  S’ha enviat un correu al equip del HackLab explicant el projecte per si ens poden ajudar ja sigui
amb la seva experiència i/o amb material.  

	
* **Nerea Update **
	*  Refresh de codi: un cop funcionava el codi anterior, s’ha implementat un codi més funcional i curt . S’ha passat tot el codi amb arrays per reduir-lo i automatizar les coses amb bucles de for que passen per les arrays de intensitats, lectura de sensors, etc.
S’ha afegit la funcionalitat de que si hi ha dos o més  persones no es distorsionin els estats de les llums, és a dir, que una no modifiqui la intensitat de l’altre si les dues estan a HIGH->0.8.
S’ha afegit una funció ShutDown que apaga totes les llums si tots els sensors estan a LOW, és a dir, si cap detecta moviment.  



* **Lluis and Nerea Update **
	*  Aquí deixem la versió final del codi i hardware de l’arduino amb 4 sensors i 4 llums:
https://www.tinkercad.com/things/6ituJ1oLcwi-1a-4s-4l/editel?sharecode=7lgt-nfB8qonB0gXAKp0kiDv3nf1E03QjrsRQ_q7yXw=   
[![y45_6546.png](https://s33.postimg.cc/8o3xakowv/y45_6546.png)](https://postimg.cc/image/oz416w1ej/)  

	

* **Nerea Update **
	*  S’ha afegit un fotorresistor a la simulació de l’Arduino que detecta la quantitat de llum natural.
S’ha modificat el codi per tal de que ara, depenen de la quantitat de llum natural que detecta el
fotoresistor, si supera el threshold disminueix, sino manté les intensitats de les leds.
Per altra banda, s’ha modificat una mica el codi de manera que ara les intensitats queden
registrades a un array y és encara més funcional perque aixi és més facil accedir i canviar-les.
Aquí deixem el codi i hardware final d’aquesta part:  
https://www.tinkercad.com/things/56ca9yChnGO-1a-4s-4l-1f/editel?sharecode=ShMxFQXB7bUW5jGQkbX97-Saq4hGW_9LnwYevtdQHgo=
[![4353455.png](https://s33.postimg.cc/a35hzdib3/4353455.png)](https://postimg.cc/image/frbsq9mnf/)  

	
* **Lluis Update **
	*  S’han connectat dos arduinos i s’han fet proves. Al conectar dos arduinos ho estem fent distribuït. S’estan provant moltes coses , no s’estava molt segurs de quin protocol fer servir ni com comunicar-los. Hem buscat molta informació. En principi ho farem amb comunicació I2C amb la llibreria Wire, tot i que no sabem molt bé com funciona.  
	
* **Luis and Nerea Update **
	*  S’han solucionat errors que feien que la simulació no funcionés bé, ha dut molta estona. A vegades es fa molt difícil solucionar errors en aquest tipus de programació ja que s’ha de pensar d’una manera diferent a un codi normal. S’ha de tenir en compte que no és un programa seqüencial fins el final, sinó que és un bucle que es va repetint continuament  i això de vegades provoca errors... S’ha proposat idea de llums d’emergència que  sempre es troben enceses com a mínim a un 5% quan l’umbral de nit es sobrepassa.  

* **Nerea Update **
	*  S’ha implementat la llum d’emergència en un sol arduino que s’encén a un 5% si la llum captada
és menor que el threshold_night que hem establert a 100 dins un interval de [54,974]. Per fer això,
s’han hagut de fer canvis a algunes funcions.  
	
* **Lluis Update **
	*  Mentre la Nerea s’encarregava d’això, s’ha avançat amb el tema de connectar dos arduinos.
Després d’aprendre tota la documentació necessaria, el que s’ha fet es fer un projecte nou
separant el sensor de llum ambiental a un altre arduino (l’esclau), i que quan es necessiti la
informació, l’arduino mestre demani la informació del sensor de llum l’arduino esclau, que li passa
“si” si es passa l’umbral de llum de dia i “no” en cas contrari. L’arduino mestre llegèix aquesta
informació i l’empra per variar la intensitat de les llums de les bombetes. Funciona però es
unidireccional. No es poden enviar missatges l’un a l’altre.
https://www.tinkercad.com/things/fetEGbjN7qz-2a-4s-4l-1f/editel?
sharecode=GgF7crp8oQBnCBhhlVENlfFEyGr_WCFfVeicFljmxGA=  
[![y_y546546.png](https://s33.postimg.cc/5u0rxaf2n/y_y546546.png)](https://postimg.cc/image/iy6c9z74b/)  

	
* **Nerea Update **
	*  S’ha modificat el main perquè no funcionava correctament. El que passava és que a cada iteració
es sobreescrivien els valors de les intensitats, i no acabava de funcionar correctament, és a dir:
• primer detectava moviment i posava les leds a 80, 10,10,10
• detectava molta llum natural i baixava totes les leds 10 -> 70,0,0,0, per tant algunes
s’apagaven com havia de ser
• !!!! Però a la següent iteració tornava a posar les llums a 80,10,10,10 (i no havia de passar
perquè detecta molta llum i haurien de continuar a 70,0,0,0)
• i així fins que deixava de detectar moviment que s’apagaven totes...
Ara, ja s’ha resolt aquest problema i no s’actualitzen les led fins al final de la iteració. D’aquesta
manera no es sobreescriuen les leds (on off on off…).  
	
* **Nerea Update **
	*  Un cop hem aconseguit la comunicació entre arduinos amb el fotorresistor, hem decidit que un
arduino s’encarregui de tota una habitació, i l’altre s’encarregara del passadís,el lavabo y queda
pendent una segona habitació.
Ara mateix, un sol arduino s’encarrega de les llums segons el moviment, llum natural i llum
d’emergència satisfactòriament.   

* **Lluis y Nerea Update **
	*  S’ha afegit el sensor del lavabo i la seva llum.
També s’han afegit tres sensor de distància per anar il·luminant el camí del passadís quan hi ha
molt poca llum ambiental (nit)
La cosa queda:
Arduino 1
-S’encarrega de l’habitació, llum emergència i fotorresistor i es comunica amb l’arduino 2 per
activar les llums del passadís a la nit.
-També tindrà una llum que dirà si el lavabo està ocupat , de manera que l’arduino 2 li dirà al 1 si
esta ocupat o no amb una funció que tindrà el 1.
Arduino 2
S’encarrega de les llums del passadís a mesura que detecten “moviment” mitjançant uns sensors
de distància. Quan detecten menor distància de la habitual és que ha passat algú i s’encenen les
llums del passadís a mesura que s’avança. Això només passarà quan sigui de nit i el fotorresistor
de l’arduino 1 li hagi comunicat al 2.
També s’encarrega del lavabo. Quan detecta que algú entra s’encén la llum i li diu a l’habitació que
està ocupat.
S’ha estat investigant com comunicar-se entre dos arduinos sense la jerarquia mestre/esclau (o
amb aquesta, però bidireccionalment).
Ara mateix ja esta fet la simulació de hardware de bedroom, hallway, restroom. Tot i que hi ha
problemes de comunicació bidireccional.  
[![reret.png](https://s33.postimg.cc/a4ffsbmkf/reret.png)](https://postimg.cc/image/cyil5roqj/)

	
* **Lluis and Nerea Update **
	*  S’ha investigat més sobre la comunicació bidireccional sense jerarquia i després de varies hores el
	Lluis ha trobat una font on semblava que tant un arduino com l’altre enviaven i rebien missatges!!!
	S’ha trobat amb el protocol I2C que empra la llibreria Wire. S’ha investigat durant una estona
	sobre la llibreria Wire, i totes les seves funcions i possibilitats. (amb aquest protocol es amb el que
	es va aconseguir la comunicacio unidireccional en un principi).
	Hem fet una prova amb dos leds i dos arduinos que segons el que rebien de l’altre arduino
	encenien la seva llum(Nerea) i ha funcionat, però curiosament si intercanviem el codi entre els dos
	arduinos deixa de funcionar !!
	https://www.tinkercad.com/things/aGVw6NMMcKK-exquisite-albar-lappi/editel
	Mentre, s’ha actualitzat el codi amb aquest protocol de comunicació (Lluis) de manera que el
	passadís s’encengui quan es detecta molt poca llum i passa una persona i que s’encengui una
	llumeta a l’habitació si hi ha gent al bany. S’ha fet amb transmisions (el master fa una transmisió
	amb l’esclau i li pot escriure informació), i amb requests(el master fa una request i el fill escriu una
	resposta al master). D’aquesta manera podem obtenir una comunicació bidireccional, ja que fins
	ara tan sols l’haviem aconseguida unidireccional i estàvem molt frustrats perque no trobàvem més
	informació. Teòricament ja s’ha canviat tot el codi per aconseguir la comunicació entre els dos
	arduinos, però no funciona. Demà ho acabarem de treure segur!  

	*  https://www.tinkercad.com/things/aGVw6NMMcKK-exquisite-albar-lappi/editel?sharecode=xneBOzIORA2uwlmqdJxekuzA0W7-PeP21wlt92maFiU=?sharecode=h5_RK_KSJjozIQYMV0Ffc9dOZuG5bqNQc1tbizGDuxE=   


	*  [![r_tetrhtfgh.png](https://s33.postimg.cc/kehurn4r3/r_tetrhtfgh.png)](https://postimg.cc/image/6xkw8ruff/)  


* **Lluis and Nerea Update **
	*  S’ha estudiat molt el codi. S’ha provat (amb l’últim projecte del que hi ha foto) que envii un missatge d’anada a un arduino i un missatge de tornada (s’ha anyadit un sensor de distància i s’han canviat alguns altres aspectes de codi per fer proves). Aquest petit hardware i petita distribució funciona i amb ‘poc’ temps. Però al passar-ho al projecte ‘master’ sembla que funciona pero tarda moltíssim. Després d’investigar molt hem descobert que amb el que es tarda molt és amb fer la transmissió, i que l’arduino 2 (esclau, encarregat del lavabo i passillo) per motius desconeguts va molt lentament. Hem detectat que es possible que es produeixi l’efecte ‘bottleneck’ i hem mirat de solucionar-ho posant delays i intentant esperar a que s’envii el que intentem transmetre però no ho aconseguim. Es veu que l’arduino 2 corre tan lentament que fa petar l’arduino 1, i a més la comunicació no es fa correctament ja que l’arduino 1 escriu al buffer per la transmissió, i abans que l’arduino 2 el llegeixi, ja ha tornat a llegir. Vaja que no sabem com sincronitzar això tampoc. Portem unes 5 sessions encallats amb això i ja estem super “rallats”.  

	A part d’això, s’han fet canvis al codi perquè fós més curt i trigués menys en executar-se ja que sembla que la comunicació triga exageradament i que la resta de codi estigui bé, però la bombeta que ens avisa si el bany està ocupat no s’encén. (transmissió pare->fill funciona tot i que molt lentament, però fill->pare es corromp a causa de la lenta velocitat del fill que no aconseguim arreglar.  A part, transmissió molt lenta).
	La conclusió d’ara és que tampoc sabem si el demés funciona perquè tot ens peta.

	
* **Lluis and Nerea Update **
	*  És dimecres de l’última setmana per entregar el treball i anem molt estressats, no aconseguim sortir dels problemes anomenats a l’últim escrit. Hem parlat amb l’Arash i hem decidit fer un anàlisi científic del nostre problema i veure si el podem solucionar:
S’ha decidit que es farà un anàlisi del següent per documentar el problema i descobrir la causa, tot i que no el poguem solucionar abans d’entregar el treball:
1- anàlisi dels temps que es tarda en fer segons quines coses, per veure els retards
2- Fer que la comunicació es faixi una vegada cada 50 loops, per veure si així no ens va tan lentament.
3- Passar a hardware tot el que poguem per veure si es problema del simulador  

** Anàlisi del temps: **

- *PASSILLO*
Amb tres sensors de distància del passadís tarda 0.632 ds en encendre’s les llums del passillo desde que comença l’execució.
Amb un sensor tarda exactament el mateix 0.632 ds.

- *LAVABO*
La llum del lavabo en sí tarda 0.632 ds per encendre’s des de que començem l’execució.
La llum de l’habitació que ens avisa de si el bany està ocupat tarda 00:00:06.101 (desde que s’encén la llum del lavabo fins que s'encén la llumeta de l’habitació) a encendre’s. Per això s’ha afegit un delay de 6000 ds que garanteixen la correcta comunicació entre arduinos sense solapar les escriptures i lectures del wire (el buffer). Per tot això, hem conclòs que no és problema del hardware (possibilitat de que els sensors de distancia relentitzessin) ni del processador sinó del temps de transmissió o de que la màquina 2 va lenta sense motiu aparent (falta solucionar).

- *EMERGÈNCIA*
La llum d’emergència tarda uns 0.1 ds en encendre’s desde el comensament del loop.

- *LLUMS  ROOM*
S’encenen de manera bastant instantània.

- *TRANSMISSIÓ*
Em evaluat i comprovem que l’arduino 1 tarda en fer transmissió, request i lectura de request el temps que haguem posat de delay entre transmissió i request. Per tant el que va lentament es l’arduino 2 en fer la seva feina i no l’arduino 1 fent la transmissió.
Hem anat ajustant el delay tot el que hem pogut disminuint el seu valor de 6000 a 2000 (que és el que tarda en executar-se l’arduino 2) . Ho hem ajustat debuggant i disminuint el valor del delay haviem de seguir veient que l’arduino 1 llegia bé el missatge del buffer escrit per l’arduino 2 (i que no es llegia el que ell mateix acabava d’escriure, és a dir, li donem temps a l’arduino 2  per a que llegeixi i escrigui el que li toca).*

	
2. Reducció de vegades que fem la transmissió en 1 loop:
*  Com que el message passing tarda tant a causa de l’arduino 2, hem decidit reduir el nombre de vegades que fem la comunicació entre arduinos a 1 cada 50 loops. D’aquesta manera podrem fer que la resta de hardware funcioni (que no te res a veure amb comunicació més instantàniament (Arash’s idea ;) ).
Ara mateix hem aconseguit arreglar-ho tot i que tot funcioni, tot i que no funciona amb tota la velocitat que voldríem, però funciona i es pot veure bé. Pensàvem que no ho aconseguirem i si ho hem aconseguit! Estem molt orgullosos.

3. Passar-ho tot a hardware:
*  Demà el Lluis mirarà de conectar el hardware i fer-ho aviam si funciona, tot i que la simulació ja queda ben acabada i funcional!.



* **Lluis **
	*Avui (dijous) m’he passat tot el matí passant a hardware tot el que he pogut passar de la simulació anterior amb el hardware que s’ha pogut aconseguir. Hem hagut de prescindir dels sensors pir i he hagut de pensar i canviar coses per fer-ho amb tan sols un PIR, i utilitzant el sensor de distància del passadís com a sensor PIR per a veure si funciona la comunicació.
	Per tal de fer això, òbviament s’ha hagut de modificar el codi que teniem, per mig adaptar-lo al nou hardware però seguint poguent provar la comunicació (que es el que ens interessa). Els dos codis compilen sense problemes a l’IDE d’arduino però a la hora de passar els programes als respectius arduinos, curiosament al conectar l’arduino uno de l’arash(oficial), deixa de detectar el meu arduino (pirata), en contra de detectar els dos, que es el que hauria de fer… no ho entenc, seguiré provant i cercant solucions per internet, per què de moment no he pogut provar el hardware.
A part d’això s’han aferrat els codis de les diferents versions del codi, fotos i un video.*
[![rol8ut4ntoer.png](https://s33.postimg.cc/aty8537sv/rol8ut4ntoer.png)](https://postimg.cc/image/6xkw93mt7/)
* **Lluis:** no s’ha aconseguit solucionar el problema de detecció dels dos arduinos… queda pendent per l’estiu solucionar-ho i implementar una alarma al nostre sistema inteligent (la Nerea i jo volem seguir amb el projecte d’arduino). S’ha gravat la pantalla simulant el nostre hardware i codi finals per al video final del projecte.


	



 